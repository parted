/******************************************************************************
 * @file            parted.h
 *****************************************************************************/
#ifndef     _PARTED_H
#define     _PARTED_H

struct part_info {

    int status, type;
    long start, end;

};

struct parted_state {

    const char *boot, *outfile;
    
    struct part_info **parts;
    long nb_parts;
    
    long image_size;
    int chs_align, update;

};

extern struct parted_state *state;
extern const char *program_name;

#endif      /* _PARTED_H */
